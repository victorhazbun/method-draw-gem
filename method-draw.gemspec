$:.push File.expand_path("../lib", __FILE__)

require "method_draw/version"

Gem::Specification.new do |s|
  s.name        = "method_draw"
  s.version     = MethodDraw::VERSION
  s.authors     = ["Jeff Saracco"]
  s.email       = ["jeffsaracco@gmail.com"]
  s.homepage    = "https://bitbucket.org/alacalc/method-draw-gem"
  s.license     = "MIT"
  s.summary     = "Easy Method-Draw integration for any rails application."
  s.description = "MethodDraw adds another form input tag to your application designed to allow the user to create or edit svg images through Method-Draw."

  s.files = Dir["{app,lib,vendor}/**/*"] + ["MIT-LICENSE", "Rakefile", "README.md"]
  s.test_files = Dir["spec/**/*"]

  s.add_dependency "rails", ">= 3.1"
  s.add_dependency "nokogiri"

  s.add_development_dependency "sqlite3"
  s.add_development_dependency "minitest-rails"
  s.add_development_dependency "minitest-rails-capybara"
end
