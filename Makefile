update_method_draw:
	# Clone Method Draw
	rm -fR tmp; mkdir tmp; 
	cd tmp; git clone ssh://git@bitbucket.org/alacalc/Method-Draw.git
	cd tmp/Method-Draw; git stash; git pull; make -B all
	# Copy over assets files and add them to the git repo
	git rm -rf vendor/assets/javascripts/method-draw-js
	mkdir -p vendor/assets/javascripts/method-draw-js
	cp -R tmp/Method-Draw/method-draw/* vendor/assets/javascripts/method-draw-js
	git add vendor/assets/javascripts/method-draw-js
	git commit -m "Update method draw"
	git push origin master
